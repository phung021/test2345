/**
  ******************************************************************************
  * @file    LwIP/LwIP_HTTP_Server_Netconn_RTOS/Inc/main.h 
  * @author  MCD Application Team
  * @brief   Header for main.c module
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2017 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32h7xx_hal.h"
#include "stm32h7xx_nucleo.h"

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/

/*Static IP ADDRESS*/
#define IP_ADDR0   192
#define IP_ADDR1   168
#define IP_ADDR2   0
#define IP_ADDR3   10
   
/*NETMASK*/
#define NETMASK_ADDR0   255
#define NETMASK_ADDR1   255
#define NETMASK_ADDR2   255
#define NETMASK_ADDR3   0

/*Gateway Address*/
#define GW_ADDR0   192
#define GW_ADDR1   168
#define GW_ADDR2   0
#define GW_ADDR3   1

#define USARTx                           USART6												//
#define USARTx_CLK_ENABLE()              __HAL_RCC_USART6_CLK_ENABLE()						//
#define USARTx_RX_GPIO_CLK_ENABLE()      __HAL_RCC_GPIOC_CLK_ENABLE()						//
#define USARTx_TX_GPIO_CLK_ENABLE()      __HAL_RCC_GPIOC_CLK_ENABLE()						//
																							//
#define USARTx_FORCE_RESET()             __HAL_RCC_USART6_FORCE_RESET()						//
#define USARTx_RELEASE_RESET()           __HAL_RCC_USART6_RELEASE_RESET()					//
																							//
/* Definition for USARTx Pins */															//
#define USARTx_TX_PIN                    GPIO_PIN_6											//
#define USARTx_TX_GPIO_PORT              GPIOC												//
#define USARTx_TX_AF                     GPIO_AF7_USART6									//
#define USARTx_RX_PIN                    GPIO_PIN_7											//
#define USARTx_RX_GPIO_PORT              GPIOC												//
#define USARTx_RX_AF                     GPIO_AF7_USART6									//
																							//
/* Definition for USARTx's NVIC */															//
#define USARTx_IRQn                      USART6_IRQn										//
#define USARTx_IRQHandler                USART6_IRQHandler									//
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */  


#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */


/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
